При деплое разворачиваются Apache Airflow, Postgres и Spark.

Описание DAGa:
* Из API забираются данные о стоимости BTC и помещаются в Postgres базу данных.

Особенности:
* Установлен airflow code editor, позволяющий редактировать код DAGa из админки Airflow.
* Postgres доступен на 5433 порту хоста.

Требования: 
* Установлен docker, доступно интернет-соединение.

Деплой:
* Запускаем демон докера, если он не запущен.
```shell
sudo dockerd &
```
* Копируем репозиторий.
```shell
git clone https://gitlab.com/devops501303/itmo_devops_labs.git
```
* Поднимаем Airflow с помощью docker compose.
```shell
docker-compose up -d
```
* Airflow будет доступен по адресу http://localhost:8080, логин admin, пароль admin.
* Spark будет доступен по адресу http://localhost:4040.
* Для работы со Spark'ом необходимо добавить подключение в Airflow (тип Spark, хост spark://spark-master, порт 7077).