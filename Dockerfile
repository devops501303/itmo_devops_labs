FROM apache/airflow:2.7.1
WORKDIR /opt/airflow

USER root
RUN apt update && apt -y install procps default-jre

USER airflow
COPY ./dags/* ./dags/
COPY ./spark/* ./spark/
RUN pip install --no-cache pandas sqlalchemy airflow-code-editor apache-airflow-providers-apache-spark
