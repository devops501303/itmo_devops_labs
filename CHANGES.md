Лабораторная №1:
Добавлены библиотеки sqlalchemy, pandas и airflow-code-editor.
Изменены данные для входа в airflow на admin:admin.

Лабораторная №2:
Добавлен модуль для работы со Spark'ом в Airflow.
Добавлен Apache Spark (1 master, 1 worker).
Добавлен dag, выполняющий spark job'у.
