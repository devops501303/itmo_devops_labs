import pendulum, os
from airflow.decorators import dag, task
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator


@dag(start_date=pendulum.now(), schedule='@daily')
def spark_dag():
    spark_job = SparkSubmitOperator(task_id = 'spark_job', 
                        application = '/opt/airflow/spark/spark.py', 
                        name = 'spark_job', 
                        conn_id = 'spark_local')
    spark_job
spark_dag()
