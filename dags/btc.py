import requests
import pandas as pd
import sqlalchemy
import pendulum
from airflow.decorators import dag, task 

@dag(start_date=pendulum.now(), schedule='@hourly')
def download_last():
    
    @task()
    def fetch():
        api_url = 'https://api.api-ninjas.com/v1/cryptoprice?symbol=BTCUSDT'
        response = requests.get(api_url, headers={'X-Api-Key': 'I8Q3GhLh78BiL5VQBC9c/A==UTBMmtXsPiynpb7g'})
        fetched = pd.DataFrame([response.json()])
        fetched['timestamp'] = pd.to_datetime(fetched['timestamp'], unit='s', origin='unix')
        return fetched

    @task()
    def save(fetched):
        engine = sqlalchemy.create_engine('postgresql://airflow:airflow@postgres/airflow')
        with engine.connect() as connection:
            connection.execute(sqlalchemy.text('create schema if not exists crypto;'))
            fetched.to_sql(con= connection,
                    name='btc_price',
                    schema='crypto',
                    if_exists='append',
                    index=False)
            
    save(fetch())
    
download_last()