import pyspark
from pyspark.sql import SparkSession

spark = SparkSession.builder.master("spark://spark-master:7077") \
                    .appName('Spark job example') \
                    .getOrCreate()
df = spark.createDataFrame([{'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676974},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676975},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676976},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676977},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676978},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676979},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676980},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676981},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676982},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676983},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676984},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676984},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676986},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676987},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676987},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676988},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676989},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676990},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676991},\
                            {'symbol': 'BTCUSDT', 'price': '40852.49000000', 'timestamp': 1705676992}])
df.show()
df.agg({'price':'mean'}).show()
spark.stop()